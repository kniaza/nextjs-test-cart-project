import { useEffect } from 'react'
import { GetServerSideProps } from 'next'
import { getTestData } from '../utils/test-data'
import { useStoreon } from '../store'
import { IProduct } from '../interfaces/product'
import { IName } from '../interfaces/names'
import List from '../components/List'
import Layout from '../components/Layout'

type Props = {
  products: IProduct
  names: IName
}

const IndexPage = (props: Props) => {
  const { dispatch, products, usdRate } = useStoreon(
    'products',
    'names',
    'usdRate'
  )

  useEffect(() => {
    dispatch('setInitial', {
      products: props.products.Value.Goods,
      names: props.names,
    })
  }, [])

  return (
    <Layout>
      <List products={products} usdRate={usdRate} />
    </Layout>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {
  const res = await getTestData()

  return {
    props: {
      products: res?.products,
      names: res?.names,
    },
  }
}

export default IndexPage
