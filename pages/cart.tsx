import Layout from '../components/Layout'
import ListItem from '../components/ListItem'
import { useStoreon } from '../store'
import usePrevious from '../utils/hooks/usePrevious'

export default function Cart() {
  const { dispatch, cart, usdRate, names } = useStoreon(
    'cart',
    'usdRate',
    'names'
  )
  const prevUsdRate = usePrevious(usdRate)

  console.log(cart)
  return (
    <Layout>
      <div>
        {cart.length === 0 && <h3>Cart is empty.</h3>}
        <ListItem
          mode="cart"
          products={cart}
          names={names}
          currency={{ usdRate, prevUsdRate }}
          onRemove={(product) => {
            dispatch('removeCartItem', product)
          }}
        />
        <hr />
        <p>
          <span>Price total: </span>
          <span>
            ${' '}
            {cart.reduce((prev, curr) => {
              return prev + curr.C * curr.P * usdRate
            }, 0)}
          </span>
        </p>
      </div>
    </Layout>
  )
}
