import { NextApiRequest, NextApiResponse } from 'next'
import products from '../../../data/products.json'

const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    res.status(200).json(products)
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message })
  }
}

export default handler
