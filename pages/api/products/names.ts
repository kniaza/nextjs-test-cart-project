import { NextApiRequest, NextApiResponse } from 'next'
import names from '../../../data/names.json'

const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    res.status(200).json(names)
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message })
  }
}

export default handler
