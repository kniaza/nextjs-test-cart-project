import React from 'react'
import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import Link, { LinkProps } from 'next/link'

export type ButtonLinkProps = Omit<IconButtonProps, 'href' | 'classes'> &
  Pick<LinkProps, 'href' | 'as' | 'prefetch'>

const MenuIconButton = React.forwardRef<ButtonLinkProps, any>(
  ({ href, as, prefetch, ...props }, ref) => (
    <Link href={href} as={as} prefetch={prefetch} passHref>
      <IconButton ref={ref} {...props} />
    </Link>
  )
)

export default MenuIconButton
