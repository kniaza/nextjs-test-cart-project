import { Container } from '@material-ui/core'
import Head from 'next/head'
import { useStoreon } from '../store'
import { randomNumbersBetween } from '../utils/randomUsdRate'
import { getTestData } from '../utils/test-data'
import { useEffect } from 'react'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Badge from '@material-ui/core/Badge'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import MenuButton from './common/MenuButton'
import MenuIconButton from './common/MenuIconButton'

type Props = {
  children: React.ReactNode
  title?: string
}

let REQUEST_INTERVAL: ReturnType<typeof setInterval>
const requestIntervalTime = 15000
let USD_RATE_INTERVAL: ReturnType<typeof setInterval>
const usdRateIntervalTime = 20000

export default function Layout({ children, title = 'Test Cart App' }: Props) {
  const classes = useStyles()
  const { dispatch, cart, products, names, usdRate } = useStoreon(
    'cart',
    'usdRate',
    'names',
    'products'
  )

  const fetchData = async () => {
    const res = await getTestData()
    dispatch('setInitial', {
      products: res?.products?.Value?.Goods || [],
      names: res?.names || {},
    })
  }

  useEffect(() => {
    if (!products || !names) {
      fetchData()
    }
  }, [])

  useEffect(() => {
    console.log({ products, names })

    REQUEST_INTERVAL = setInterval(fetchData, requestIntervalTime)

    return () => {
      clearInterval(REQUEST_INTERVAL)
    }
  }, [products, names])

  useEffect(() => {
    clearInterval(USD_RATE_INTERVAL)
    USD_RATE_INTERVAL = setInterval(() => {
      const randomNumber = randomNumbersBetween(50, 80)
      dispatch('setUsdRate', randomNumber)
    }, usdRateIntervalTime)
  }, [usdRate])

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <AppBar position="static">
        <Toolbar>
          <span>Test Marketplace</span>

          <div className={classes.sectionDesktop}>
            <MenuButton href="/" color="inherit">
              <span>Home</span>
            </MenuButton>
            <MenuIconButton
              href="/cart"
              aria-label="show 17 new notifications"
              color="inherit"
            >
              <Badge badgeContent={cart.length} color="secondary">
                <ShoppingCartIcon />
              </Badge>
            </MenuIconButton>
          </div>
        </Toolbar>
      </AppBar>

      <Container maxWidth="md">
        <>{children}</>
      </Container>
    </div>
  )
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },

    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
        flex: 1,
        justifyContent: 'flex-end',
      },
    },
  })
)
