import React from 'react'
import { GoodsType } from '../interfaces/product'
import classnames from 'classnames'

import Button from '@material-ui/core/Button'
import {
  colors,
  createStyles,
  Grid,
  makeStyles,
  Theme,
} from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pricePositive: {
      color: colors.green[300],
    },
    priceNegative: {
      color: colors.red[300],
    },
  })
)

type Props = {
  mode: 'cart' | 'default'
  products: GoodsType[]
  names: IName
  categoryId: string
  cart: GoodsType[]
  currency: {
    usdRate: number
    prevUsdRate: number
  }
  onRemove(product: GoodsType): void
  onAdd(product: GoodsType): void
}

export default function ListItem({
  mode = 'default',
  products,
  names,
  cart,
  currency,
  onRemove,
  onAdd,
}: Props) {
  const classes = useStyles()
  return (
    <>
      {products.map((product: GoodsType) => {
        const title = names?.[product.G]?.['B']?.[product.T]?.['N']
        const price = product.C
        const quantity = product.P
        const key = `${product.G}-${product.T}`
        const isInCart = cart?.filter((item) => item.T === product.T)

        const isCartMode = mode === 'cart' || isInCart.length

        if (!title) return
        return (
          <Grid key={key} container xs={12}>
            <Grid item xs={6}>
              <p>{title}</p>
            </Grid>
            <Grid item container justify="center" xs={4}>
              <p>
                <span
                  className={classnames({
                    [classes.pricePositive]:
                      currency.usdRate < currency.prevUsdRate,
                    [classes.priceNegative]:
                      currency.usdRate > currency.prevUsdRate,
                  })}
                >
                  ${(price * currency.usdRate).toFixed(2)}
                </span>
                <i>*{quantity}</i>
              </p>
            </Grid>
            <Grid item container justify="center" xs={2}>
              <p>
                <Button
                  variant="contained"
                  color={isCartMode ? 'secondary' : 'primary'}
                  onClick={() => {
                    if (isCartMode) {
                      onRemove(product)
                    } else {
                      onAdd(product)
                    }
                  }}
                >
                  {isCartMode ? 'Remove' : 'Add cart'}
                </Button>
              </p>
            </Grid>
            <hr style={{ backgroundColor: '#555' }} />
          </Grid>
        )
      })}
    </>
  )
}
