import React, { useCallback, useMemo } from 'react'
import { GoodsType } from '../interfaces/product'
import { useStoreon } from '../store'
import usePrevious from '../utils/hooks/usePrevious'
import ListItem from './ListItem'

type Props = {
  products: GoodsType[]
  usdRate: number
}

const List = ({ products, usdRate }: Props) => {
  const { dispatch, cart, names } = useStoreon('cart', 'names')
  const prevUsdRate = usePrevious(usdRate)

  const categories = useMemo(() => {
    if (!names) return {}
    return {
      categoriesId: Object.keys(names),
    }
  }, [products, names])

  const handleAddCart = useCallback((product) => {
    dispatch('addCartItem', product)
  }, [])

  const handleRemoveCart = useCallback((product) => {
    dispatch('removeCartItem', product)
  }, [])

  console.log('render', cart)

  return (
    <div>
      {categories.categoriesId?.map((categoryId) => (
        <div key={categoryId}>
          <h2>{names[categoryId]?.['G']}</h2>
          <ListItem
            products={products.filter(
              (item) => item.G === parseInt(categoryId)
            )}
            names={names}
            cart={cart}
            currency={{ usdRate, prevUsdRate }}
            onRemove={handleRemoveCart}
            onAdd={handleAddCart}
          />
        </div>
      ))}
    </div>
  )
}

export default List
