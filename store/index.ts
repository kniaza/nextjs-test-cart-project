import { createContext } from 'react' // or preact

import { createStoreon } from 'storeon'
import { customContext } from 'storeon/react' // or storeon/preact
import { persistState } from '@storeon/localstorage'

import {
  productsModule,
  State as ProductsState,
  Events as ProductEvents,
} from './products'
import {
  cartModule,
  State as CartState,
  Events as CartEvents,
} from './products/cart'

// compose local stores types to create global store
export type State = ProductsState & CartState
export type Events = ProductEvents & CartEvents

export const store = createStoreon<State, Events>([
  productsModule,
  cartModule,
  persistState(['cart']),
])

const CustomContext = createContext(store)

// useStoreon will automatically recognize your storeon store and event types
export const useStoreon = customContext(CustomContext)
