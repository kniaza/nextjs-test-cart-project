import { StoreonModule } from 'storeon'
import { GoodsType } from '../../interfaces/product'

// State structure
export interface State {
  cart: GoodsType[]
}

// Events declaration: map of event names to type of event data
export interface Events {
  addCartItem: GoodsType
  removeCartItem: GoodsType
}

export const cartModule: StoreonModule<State, Events> = (store) => {
  store.on('@init', () => ({ cart: [] }))

  store.on('addCartItem', (state, newItem) => ({
    cart: [...state.cart, newItem],
  }))

  store.on('removeCartItem', (state, product) => {
    return {
      cart: state.cart.filter((item) => item.T !== product.T),
    }
  })
}
