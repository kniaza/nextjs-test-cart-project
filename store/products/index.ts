import { StoreonModule } from 'storeon'
import { IName } from '../../interfaces/names'
import { GoodsType } from '../../interfaces/product'

// State structure
export interface State {
  products: GoodsType[]
  names: IName | null
  usdRate: number
}

// Events declaration: map of event names to type of event data
export interface Events {
  set: []
  setInitial: Pick<State, 'products' | 'names'>
  setUsdRate: number
}

export const productsModule: StoreonModule<State, Events> = (store) => {
  store.on('@init', () => ({ products: [], names: null, usdRate: 50 }))

  store.on('set', (state, newProduct) => ({
    products: [...state.products, ...newProduct],
  }))

  store.on('setInitial', (_, initialProducts) => ({
    products: initialProducts.products || [],
    names: initialProducts.names || null,
  }))

  store.on('setUsdRate', (_, rate) => ({
    usdRate: rate,
  }))
}
