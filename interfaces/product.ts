export type GoodsType = {
  B: boolean
  C: number // цена товара в долларах
  CV: null
  G: number // id группы
  P: number // количество товара
  Pl: null
  T: number // id товара
}

export interface IProduct {
  Error: string
  Id: number
  Success: boolean
  Value: {
    Goods: GoodsType[]
  }
}
