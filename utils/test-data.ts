import axios from 'axios'

import namesData from '../data/names.json'
import productsData from '../data/products.json'

export type ReturnTestData = {
  products: typeof productsData
  names: typeof namesData
}

const getTestData = async (): Promise<ReturnTestData | null> => {
  try {
    const productsReq = axios.get('http://localhost:3000/api/products')
    const namesReq = axios.get('http://localhost:3000/api/products/names')

    const res = await Promise.all([productsReq, namesReq])

    return {
      products: res[0].data,
      names: res[1].data,
    }
  } catch (error) {
    return null
  }
}

export { getTestData }
